import com.sun.deploy.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import static java.lang.String.join;

public class FinalProject {

    //method for convert the Arraylist<Integer> to String
    private static String convertToString(ArrayList<Integer> list) {
        StringBuilder builder = new StringBuilder();
        for (int number = 0; number < list.size(); number++) //run the list
        {
            builder.append(list.get(number));
            builder.append("");
        }
        return builder.toString();
    }

    private static void largestPossibleNumber(ArrayList<Integer> list) {

        for (int i = 0; i < list.size(); i++) { //run a value from a list
            for (int j = i + 1; j < list.size(); j++) { // run a second value from a list

                int x = list.get(i); //initialize a variable x(int) as the first value
                int y = list.get(j); //initialize a variable y(int) as a second value
                String X = Integer.toString(x); //Convert x to String
                String Y = Integer.toString(y); //Convert y to String
                // in order to solve the problem I compare each XY and YX value in the Arraylist . If XY is bigger than YX then X Y remains in the same position else swap Y and X.
                String xy = X + Y; // Define a variable xy which is the join of String variable X and Y
                String yx = Y + X; // Define a variable yx which is the join of String variable Y and X
                try {
                    int xy1 = Integer.parseInt(xy); //Convert to int the String variable xy
                    int yx1 = Integer.parseInt(yx); //Convert to int the String variable yx
                    if (xy1 > yx1) { //implement the comparison
                        int temp = x; //store in variable temp ,value x
                        int temp1 = y; //store in variable temp1 ,value y
                        list.set(i, temp); // then put the temp in position i
                        list.set(j, temp1); //put the temp1 in position j
                    } else {
                        int temp2 = y; //store in variable temp2, value y
                        int temp3 = x; //store in variable temp3, value x
                        list.set(i, temp2); // put the temp2 in position i
                        list.set(j, temp3); // put temp3 in position j
                    }
                } catch (NumberFormatException ex) {

                    System.err.println("Ilegal input"); //try catch for NumberFormatException
                }

            }
        }

        String result = convertToString(list); //call ConvertToString  for Arraylist and store in variable result
        System.out.println(result); //show the list

    }


    public static void main(String[] args) {

        ArrayList<Integer> list; //define the Arraylist list
        list = new ArrayList<>();
        list.add(420); //add values to Arraylist
        list.add(42);
        list.add(423);

        largestPossibleNumber(list); //Call methos largestPossibleNumber

    }
}

